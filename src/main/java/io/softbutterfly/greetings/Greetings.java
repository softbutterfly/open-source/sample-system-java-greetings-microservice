package io.softbutterfly.greetings;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
public class Greetings {
    private static final String template = "Hello, %s!";

    @RequestMapping("/")
    String index(@RequestParam(value = "name", defaultValue = "World") String name) {
        return String.format(template, name);
    }

    public static void main(String[] args) {
        SpringApplication.run(Greetings.class, args);
    }
}
