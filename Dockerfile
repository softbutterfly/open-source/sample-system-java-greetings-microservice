# Build Stage
# Initialize build and set base image for first stage
FROM maven:3.6.3-adoptopenjdk-11 as build

ENV MAVEN_OPTS="-XX:+TieredCompilation -XX:TieredStopAtLevel=1"

WORKDIR /app

COPY pom.xml .
RUN mvn dependency:go-offline

COPY ./src ./src
RUN mvn clean install -Dmaven.test.skip=true && mvn compile && mvn package

# New Relic stage
# Downloads new relic agent
FROM openjdk:11-jre-slim as newrelic

ARG NEW_RELIC_AGENT_VERSION=7.8.0

RUN apt-get -y update \
    && apt-get -y install \
    curl \
    unzip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN curl -q https://download.newrelic.com/newrelic/java-agent/newrelic-agent/${NEW_RELIC_AGENT_VERSION}/newrelic-java.zip --output ./newrelic.zip \
    && unzip -q ./newrelic.zip -d /opt \
    && rm ./newrelic.zip

# Final Stage
FROM openjdk:11-jre-slim

ENV NEW_RELIC_AGENT_ENABLED=true
ENV NEW_RELIC_APP_NAME='krowdy-greetings-ms'
ENV NEW_RELIC_LICENSE_KEY='My lkicense key'
ENV NEW_RELIC_ENVIRONMENT='dev'
ENV NEW_RELIC_LOG='STDOUT'
ENV NEW_RELIC_DISTRIBUTED_TRACING_ENABLED=true

WORKDIR /app

COPY --from=build /app/target/java-greetings-microservice-0.0.1-SNAPSHOT.jar /app/java-greetings-microservice-0.0.1-SNAPSHOT.jar
COPY --from=newrelic /opt/newrelic/newrelic.jar /app/newrelic.jar

EXPOSE 8080

ENTRYPOINT ["java", "-javaagent:/app/newrelic.jar", "-Dnewrelic.agent_enabled=${NEW_RELIC_AGENT_ENABLED:-false}", "-Dnewrelic.environment=${NEW_RELIC_ENVIRONMENT:-dev}", "-jar" , "/app/java-greetings-microservice-0.0.1-SNAPSHOT.jar"]
