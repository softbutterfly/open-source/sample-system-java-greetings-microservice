![Sample-Code](https://gitlab.com/softbutterfly/open-source/open-source-office/-/raw/master/banners/softbutterfly-open-source--banner--sample-code.png)

# New Relic Demo: APM for Java Spring API Rest

This repository contains a sample project to demonstrate how to use New Relic APM for Java Spring API Rest in a Docker environment.

## How to use

1. Clone this repository

```bash
git clone git@gitlab.com:softbutterfly/open-source/sample-system-java-greetings-microservice.git
```

2. Copy environment file

```bash
cp .env.template .env
```

3. Build the project

```bash
docker compose build \
    --build-arg NEW_RELIC_AGENT_VERSION="your new relic agent version" \
    backend
```

If you are not using `docker compose`, you can build the image with the following command:

```bash
docker build \
    --build-arg NEW_RELIC_AGENT_VERSION="your new relic agent version" \
    -t your-docker-label \
    .
```

4. Run the project

```bash
docker compose up 
```
